import React, { useState } from "react";
import $ from "jquery";
import { useAppContext } from "./libs/contextLib";


function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const { userHasAuthenticated, setUserInfo } = useAppContext();


    function validateForm() {
        return username.length > 0 && password.length > 0;
    }

    function handleLogin(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "/auth/login",
            data: JSON.stringify({ username: username, password: password }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                $("form").fadeOut(1000, () => {
                    userHasAuthenticated(true);
                    setUserInfo(data.userInfo);
                });
                },
            error: function(errMsg) {
                alert("Login Failed.");
            }
        });

    }
    function handleSignup(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "/auth/signup",
            data: JSON.stringify({ username: username, password: password }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                $("form").fadeOut(1000, () => {
                    userHasAuthenticated(true);
                    setUserInfo(data.userInfo);
                });
            },
            error: function(errMsg) {
                alert("Username already in use.");
            }
        });
    }

        return (
            <form>
                <input type="text" id="username" placeholder="Username"
                       value={username}
                       onChange={(e) => setUsername(e.target.value)}/>
                <br/>
                <input type="password" id="password" placeholder="password"
                       value={password}
                       onChange={(e) => setPassword(e.target.value)}/>
                <br/>
                <input type="submit" onClick={handleLogin} value="login" disabled={!validateForm()}/>
                <input type="submit" onClick={handleSignup}value="signup" disabled={!validateForm()}/>
            </form>
        );
}

export default Login;
