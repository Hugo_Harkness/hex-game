import { useContext, createContext } from "react";

export const AppContext = createContext(null);
export const GameContext = createContext(null);

export function useAppContext() {
    return useContext(AppContext);
}

export function useGameContext() {
    return useContext(GameContext);
}
