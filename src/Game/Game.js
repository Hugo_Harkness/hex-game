import React, {useState} from "react";
import Lobby from "./Lobby";
import {GameContext, useAppContext} from "../libs/contextLib";
import Board from "./Board/Board";
import WaitingRoom from "./WaitingRoom";


function Game() {
    const [isInGame, setIsInGame] = useState(false);
    const [isWaiting, setIsWaiting] = useState(false);
    const [game, setGame] = useState({});
    const [player, setPlayer] = useState(0);
    const [isMyTurn, setIsMyTurn] = useState(false);
    const [winner, setWinner] = useState(0);
    const { userInfo,userHasAuthenticated,setUserInfo } = useAppContext();


    //console.log("ALLLL:", isInGame, isWaiting, player, isMyTurn, winner);
    //console.log("GAME:", game);

    function handleLogout(e) {
        e.preventDefault();
        userHasAuthenticated(false);
        setUserInfo({});
        document.cookie = "JWT=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }

    return(
        <GameContext.Provider value={{
            isInGame, setIsInGame, isWaiting, setIsWaiting, game, setGame,
            player, setPlayer, isMyTurn, setIsMyTurn, winner, setWinner
        }}>
        <div>
            <h2>{userInfo.username} <button onClick={handleLogout}>Logout</button></h2>
        </div>
        <div className="game">
            {isInGame ? ( isWaiting ? ( <WaitingRoom/> ) : ( <Board/> ) ) : ( <Lobby/> )}
        </div>
        </GameContext.Provider>
    )
}

export default Game;