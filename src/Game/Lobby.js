import React, {useState,useEffect} from "react";
import $ from "jquery";
import {useGameContext} from "../libs/contextLib";


function Lobby() {
    const [games, setGames] = useState([]);
    const { setIsInGame, setIsWaiting, setGame, setPlayer} = useGameContext();

    useEffect(() => {
        const sse = new EventSource('/game/games');
        function getGameListData(data) {
           setGames(data.games);
            console.log("GAMES:", data)
        }
        sse.onmessage = e => getGameListData(JSON.parse(e.data));
        sse.onerror = () => {sse.close();}
        return () => {sse.close();};
    },[]);

    function newGame(e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "/game/new",
            success: function (data) {
                setGame(data);
                setIsWaiting(true);
                setIsInGame(true);
                setPlayer(1);
                console.log("NEW:",data)
            }
        });
    }

    function joinGame(e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "/game/join/" + e.target.id,
            success: function (data) {
                setGame(data.game);
                setIsWaiting(false);
                setIsInGame(true);
                setPlayer(2);
                console.log("JOIN:",data)
            },
            error: function (errMsg) {
                alert("Game not found");
            }
        });
    }

    return (
        <div className="lobby">
            <button onClick={newGame}>Crée une partie.</button>
            <h2>Ou rejoindre une partie :</h2>
            <ul>
                {
                    games.map((game) =>
                        <p><button onClick={joinGame} id={game.id}>{game.j1}</button></p>
                    )
                }
            </ul>
        </div>
    )
}

export default Lobby;