import React, {useEffect} from "react";
import $ from "jquery";
import './Board.css';
import {useGameContext} from "../../libs/contextLib";

function Board() {
    const { game, setGame, player, isMyTurn, setIsMyTurn,
        winner, setWinner, setIsInGame } = useGameContext();

    const size = 11;

    const borderJ1 = [];
    for (let i = 0; i < size; i++) {
        borderJ1.push(<li className='hex b1'/>);
    }

    const rows = [];
    rows.push(
        <ol className="odd">
            {borderJ1}
        </ol>
    )
    for (let i = 0; i < size; i++) {

        const spacers = [];
        for (let j = 1; j < (i+1)/2; j++) {
            spacers.push(<li className='hex spacers'/>);
        }

        const cells = [];
        for (let j = 0; j < size; j++) {
            if(game.board[i][j] === 0 && isMyTurn)
                cells.push(<li className='hex empty clickable' id={i + ' ' + j} onClick={handleClick}/>);
            else if(game.board[i][j] === 0 && !isMyTurn)
                cells.push(<li className='hex empty' id={i + ' ' + j}/>);
            else if(game.board[i][j] === 1)
                cells.push(<li className='hex j1'/>);
            else if(game.board[i][j] === 2)
                cells.push(<li className='hex j2'/>);
        }


        let parity;
        if (i % 2 === 0)
            parity = "even";
        else
            parity = "odd";

        rows.push(
            <ol className={parity}>
                {spacers}
                <li className='hex b2'/>
                {cells}
                <li className='hex b2'/>
            </ol>
        )
    }
    const spacers = [];
    for (let j = 1; j < 7; j++) {
        spacers.push(<li className='hex spacers'/>);
    }
    rows.push(
        <ol className="odd">
            {spacers}
            {borderJ1}
        </ol>
    )

    useEffect(() => {
        setIsMyTurn(game.turn === player);
    },[game.turn,player,setIsMyTurn]);

    useEffect(() => {
        console.log("game_event register");
        const sse = new EventSource('/game/game_event/' + game.id);
        function getGameEventData(data) {
            if(data.event === "gameUpdate") {
                setGame(data.game);
            }
            if(data.event === "gameEnd") {
                setWinner(data.winner);

                console.log(data)
            }
        }
        sse.onmessage = e => getGameEventData(JSON.parse(e.data));
        sse.onerror = () => {
            sse.close();
            setIsInGame(false);
        }
        return () => {sse.close();};
    }, [game.id,setGame,setWinner]);

    function handleClick(e){
        let pos = e.target.id.split(" ");
        if(isMyTurn)
        $.ajax({
            type: "POST",
            url: "/game/game_event/" + game.id,
            data: JSON.stringify({ pos: pos }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                setIsMyTurn(false);

                console.log("SEND:", data);
            }
        });
    }

    function getBoard(w){
        switch (w) {
            case 0:
                return <div className="board"> <div className="grid"> {rows} </div></div>;
            case 1:
                return <h1> The Winner is : {game.j1} </h1>;
            case 2:
                return <h1> The Winner is : {game.j2} </h1>;
            default:
                return <h1> Connection Lost with the other player ! </h1>;
        }
    }

    function handleQuit(e){
        e.preventDefault();
        setIsInGame(false);
        setWinner(0);
    }

    return (
        <div>
            <h1><span className="j1">{game.j1}</span> vs <span className="j2">{game.j2}</span></h1>
            <button onClick={handleQuit}>Quit</button>
            {getBoard(winner)}
        </div>
    );
};

export default Board;
