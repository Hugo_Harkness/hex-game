import React, {useEffect} from "react";
import {useGameContext} from "../libs/contextLib";

function WaitingRoom() {
    const { game, setIsWaiting, setIsInGame, setWinner} = useGameContext();

    useEffect(() => {
        console.log("wait")
        const sse = new EventSource('/game/wait/' + game.id);
        function getEventData(data) {
            setIsWaiting(false);
        }
        sse.onmessage = e => getEventData(JSON.parse(e.data));
        sse.onerror = () => {sse.close();}
        return () => {sse.close();};
    },[setIsWaiting,game.id]);

    function handleQuit(e){
        e.preventDefault();
        setIsInGame(false);
        setWinner(0);
    }

    return (
        <div className="waiting-room">
            <h1>Waiting for someone to join ...</h1>
            <button onClick={handleQuit}>Quit</button>
        </div>
    )
}

//LOGOUT
//LISTEN WAIT

export default WaitingRoom;