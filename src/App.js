import React, { useState } from "react";
import { AppContext } from "./libs/contextLib";

import Login from './Login';
import Game from "./Game/Game";

function App() {
    const [isAuthenticated, userHasAuthenticated] = useState(false);
    const [userInfo, setUserInfo] = useState({});

    return (
        <AppContext.Provider value={{ isAuthenticated, userHasAuthenticated, userInfo, setUserInfo }}>
        <div className="App">
            <header className="App-header">
                <h1>The Hex game</h1>
            </header>

            {isAuthenticated ? ( <Game/> ) : ( <Login/> )}


        </div>
        </AppContext.Provider>
    );
}

export default App;